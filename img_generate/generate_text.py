import json

import requests


def get_data(url: str):
    r = requests.get(url)
    return r.text


def proceed_asciinema_outputs(text: str):
    # text = text.splitlines()[1:-1]
    # result = ""
    # for line in text:
    #     line = json.loads(line)
    #     result += line[2]
    result = text.replace('\r\n', '\n').replace('，', ',')
    return result
