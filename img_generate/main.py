from .generate_text import proceed_asciinema_outputs, get_data
from .render_text import render_ansi


def generate_img_by_url(url):
    data = get_data(url)
    data = proceed_asciinema_outputs(data)
    png = render_ansi(data)
    return png
